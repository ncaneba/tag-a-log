﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bombAnimator : MonoBehaviour {
    public Animator anim;


    public void animateBomb()
    {
        anim.Play("bomb");
    }
}
