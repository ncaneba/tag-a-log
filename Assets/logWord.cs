﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class logWord : MonoBehaviour {

    public GameObject instance;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = gameObject;
        }
        else
        {
            if (instance != gameObject)
            {
                Destroy(gameObject);
            }
        }
    }
}
