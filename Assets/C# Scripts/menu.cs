﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour
{
    public GameObject SceneTransition;
    public GameObject playButton;
    public GameObject optionsButton;
    public GameObject quitButton;
    public GameObject logo;

    public void SceneChangeTransition()
    {
        SceneTransition.SetActive(true);
        playButton.SetActive(false);
        optionsButton.SetActive(false);
        quitButton.SetActive(false);
        logo.SetActive(false);

        Invoke("PlayGame", 0.8f);
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Main");
    }

   


    public void QuitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
