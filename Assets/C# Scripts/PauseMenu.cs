﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PauseMenu : MonoBehaviour
{

    public Animator anim;
    public GameObject sceneTransition1;

  

    public void MenuReset()
    {
        Time.timeScale = 1f;
    }

    public void Pause()
    {
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        Time.timeScale = 1f;
    }



    public void SceneChangeTransition()
    {
        sceneTransition1.SetActive(true);
        anim.Play("Outgoing");
        Invoke("LoadMenu", 0.1f);  
    
    }


    public void LoadMenu()
    {
        
        Debug.Log("Loading Menu....");
        SceneManager.LoadScene("Main_Menu");
    }

    public void QuitGame()
    {
        Debug.Log("Quitting Game....");
    }

    public void Replay()
    {
        SceneManager.LoadScene("Main");
    }

    public void Mute()
    {
        AudioListener.pause = !AudioListener.pause;
    }

   



}
