﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ScreenType : MonoBehaviour {

    public Button targetButton;

    public AudioSource wrongSound;
   
    public char character;

    public WordManager wordManager;
    public Word word1;
    public WordTimer wordTime;

    public keyboardAnimScript kScript;



    public vibrateScript vScript;

    public int vibrate;

    void Start () {
        //Button btn1 = targetButton.GetComponent<Button>();
        //btn1.onClick.AddListener(TaskOnClick);
 


        
            Button btn1 = targetButton.GetComponent<Button>();
            btn1.onClick.AddListener(TaskOnClick);
        

       


    }
	
	// Update is called once per frame
	void Update () {
        
        
	}

    public void TaskOnClick()
    {
        string type;
        type = targetButton.name;

        


        character = type[0];

        wordManager.TypeLetter(character);



        vibrate = PlayerPrefs.GetInt("Vibrate");

        
        if (wordManager.hasActiveWord)
        {
           
            if (character != wordManager.jesus && wordManager.wordLength != 0)
            {
                //Debug.Log("incorrect");
                kScript.animateWrong();
                wrongSound.Play();
                wordTime.wordDelay *= 0.97f;
                wordManager.combo = 0;

                if (vibrate == 0)
                {
                    Handheld.Vibrate();
                }

                

            }

            
        }

        //Debug.Log(character);
    }

    
   

}
