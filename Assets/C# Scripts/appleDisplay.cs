﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class appleDisplay : MonoBehaviour {

    public float fallSpeed = 1f;
    public appleImage imageApple;
    public Animator anim;
   
    public WordTimer wordTimer;
    public WordManager wordManager;
    public WordDisplay wordDisplay;

    public keyboardAnimScript kScript;

    
   

    void Start()
    {
        wordTimer = GameObject.FindGameObjectWithTag("GameController").GetComponent<WordTimer>();
        wordManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<WordManager>();
        wordDisplay = GameObject.FindGameObjectWithTag("Player").GetComponent<WordDisplay>();
    }

    public void RemoveApple()
    {
        imageApple.RemoveApple();
       
        Invoke("DestroyObJect", 0.5f);
    }

    public void DestroyObJect()
    {
        wordTimer.wordDelay *= 1.05f;
        DestroyMultipleObjects();
        Destroy(gameObject);
    }

    public void DestroyMultipleObjects()
    {
        wordManager.words.Remove(wordManager.activeWord);
        wordManager.words.Clear();
        wordManager.wordTime.wordDelay *= 1.12f;
        foreach (GameObject log in wordManager.player)
        {

            wordManager.hasActiveWord = false;
            wordManager.wordLength = 0;
            wordManager.score++;

            Destroy(log);
            wordManager.explodeSound.Play();
        }
        wordManager.animatelog();

        foreach (GameObject apple in wordManager.appleObject)
        {
            Destroy(apple);
        }

        


    }

  


    private void Update()
    {
        transform.Translate(0f, -fallSpeed * Time.deltaTime, 0f);
    }

    
}
