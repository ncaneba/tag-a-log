﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class incoming_transition : MonoBehaviour {

    public Animator anim;


    void Start()
    {
        anim = GetComponent<Animator>();

        if (gameObject.activeSelf)
        {
            anim.Play("Incoming");
        }
    }
  
	
	// Update is called once per frame
	void Update () {
		
	}
}
