using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class WordManager : MonoBehaviour
{
    public int score;
    public Text scoreDisplay;
    public Text scoreOver;
    public Text HighScore;
    public List<Word> words;
    

    public AudioSource explodeSound;

    public colliderManager ColManager;
    

    public WordSpawner wordSpawner;


    public appleSpawner Apple_Spawner;

    public WordTimer wordTime;

    public bool hasActiveWord;

    public Word activeWord;

    public int wordLength;

    public char jesus;

    public int combo;
    public int combo_2;

    public Animator anim;
    public GameObject newHighScore;
    public GameObject[] player;

    public GameObject[] appleObject;

    public bombAnimator bombObject;


   void Start()
    {
        HighScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
        wordLength = 0;
        combo = 0;
        combo_2 = 30;

    }

    void Update()
    {
        scoreDisplay.text = score.ToString();
        scoreOver.text = score.ToString();
        player = GameObject.FindGameObjectsWithTag("Player");
        appleObject = GameObject.FindGameObjectsWithTag("Exception");
    }

    public void AddWord()
    {
        Word word = new Word(WordGenerator.GetRandomWord(), wordSpawner.SpawnWord());
        
        //Debug.Log(word.word);

        words.Add(word);
        
    }

    public void TypeLetter(char letter)
    {
     
        
        if (hasActiveWord)
        {
            jesus = activeWord.GetNextLetter();
            wordLength++;
            if (activeWord.GetNextLetter() == letter)
            {
                activeWord.TypeLetter();
                
            }


            
        }
        else
        {
            foreach (Word word in words)
            {
                
                if (word.GetNextLetter() == letter)
                {
                   
                    activeWord = word;
                    hasActiveWord = true;
                    word.TypeLetter();
                    break;

                   
                }
                
            }
        }


        //IF THE WORD IS DONE AND CORRECT
        if (hasActiveWord && activeWord.WordTyped())
        {
            wordTime.wordDelay *= 1.01f;
            hasActiveWord = false;
            wordLength = 0;
            words.Remove(activeWord);
            combo++;
            score++;

            if (score > PlayerPrefs.GetInt("HighScore", 0))
            {
                PlayerPrefs.SetInt("HighScore", score);
                HighScore.text = score.ToString();
                ColManager.HSbool = true;
                newHighScore.SetActive(true);
            }

            if (combo > 4)
            {
                Apple_Spawner.SpawnApple();
                combo = 0;
            }


            if (score == Random.Range(10, combo_2))
            {
                Apple_Spawner.SpawnApple();
                combo_2 += 10;
            }

          

            
        }
    }


    public void animatelog()
    {
        bombObject.animateBomb();
    }



}
