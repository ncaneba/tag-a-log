﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyVillageAnim : MonoBehaviour {

    public Animator anim;

    public GameObject elf1;
    public GameObject elf2;
    public GameObject elf3;
    public GameObject elf4;
    public GameObject elf5;
    public GameObject elf6;


    public void VillageAnimation()
    {
       
        elf1.SetActive(false);
        elf2.SetActive(false);
        elf3.SetActive(false);
        elf4.SetActive(false);
        elf5.SetActive(false);
        elf6.SetActive(false);
        anim.Play("village explode");
    }
}
