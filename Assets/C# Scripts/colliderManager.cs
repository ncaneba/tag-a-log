﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class colliderManager : MonoBehaviour {

    public bool end = false;

    public bool HSbool = false;

    public GameObject gameOverMenu;
    public GameObject gameOverImage;
    public GameObject replayButton;
    public GameObject menuButton;
    public GameObject pauseButton;
    public GameObject retryButton;
    public GameObject scoreOver;
    public GameObject scoreLabel;
    public GameObject scoreDisplay;

    public destroyVillageAnim village;

    public AudioSource bgSound;
    public AudioSource destroySound;
    public AudioSource highScoreSound;



    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Exception" || other.gameObject.tag == "Bomb")
            Destroy(other.gameObject);
        

        else
        {
            Destroy(other.gameObject);
            destroySound.Play();
            village.VillageAnimation();
            
            Invoke("GameOver", 0.3f);
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0f;

        end = true;
        bgSound.Stop();
        
        activateGameOverMenu();
    }

  

    public void activateGameOverMenu()
    {
        if (end)
        {
            gameOverMenu.SetActive(true);
            gameOverImage.SetActive(true);
            replayButton.SetActive(true);
            menuButton.SetActive(true);
            scoreOver.SetActive(true);
            scoreLabel.SetActive(true);

            scoreDisplay.SetActive(false);
            pauseButton.SetActive(false);
            retryButton.SetActive(false);

            if (HSbool)
            {
                highScoreSound.Play();
            }

        }
    }


}
