﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyboardAnimScript : MonoBehaviour {

    // Use this for initialization
    public Animator anim;

    public void animateWrong()
    {
        anim.Play("wrong");
    }

   

    public void ResetHighScore()
    {
        PlayerPrefs.DeleteKey("HighScore");
        PlayerPrefs.DeleteKey("Vibrate");
    }
}
