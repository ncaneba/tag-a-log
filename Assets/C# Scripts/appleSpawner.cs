﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class appleSpawner : MonoBehaviour {

    public GameObject applePrefab;

    public Transform wordCanvas;

    public WordDisplay SpawnApple()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-1.5f, 1.5f), 8f);

        GameObject wordObj = Instantiate(applePrefab, randomPosition, Quaternion.identity, wordCanvas);

        WordDisplay wordDisplay = wordObj.GetComponent<WordDisplay>();

        return wordDisplay;
    }
}
