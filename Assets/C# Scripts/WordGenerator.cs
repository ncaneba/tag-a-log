using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordGenerator : MonoBehaviour {





    private static List<string> wordList_List = wordList_List = new List<string>
{ "albolaryo","alitaptap","abogado","alapaap","aparador",
"abaka","abaniko","abusado","agrikultura","ahas",
"agimat","aginaldo","agos","akda","aklat",
"alkalde","alon","apo","arko","aso", "awanggan" , "anluwage",
"abanderado", "abaloryo",  "abasto", "abitsuwelas",
"adhika", "abyog", "agawtulog", "agham",  "aghimuan",

"bayani","bulok","bibig","braso", "baboy","barangay","bughaw","bahin",
"balingkinitan","bahaghari","bahay","bisita", "bagabag",  "bagamundo",
"bulaklak","bukid","barko","baril", "bilnuran", "bagingan",  "bagisbis",
"balangay", "burdador", "bunyiin", "buntabay", "buntalain",  "bungkalin",
"bumulabog", "bumulahaw", "bumbon", "bumalagbag",  "buluble",

"diwa","dagat","dibdib","daliri","dalisay", "duta", "durado",
"digmaan","diwata","dalagita","dambana","damo",  "dusa",
"dila","disente","diyaryo","dugo","duwag", "dagalwak", "dagatkipot",
"duyan","disko","dighay","digmaan","dyosa", "durungawan", "duyog",

"empleyado","eskwelahan","elementarya","engkanto","estilo",
"elepante","elegante","eskenita","estado", "ehekutibo", "ehemplar",
"elesi", "enigma", "ektarya", "edukado", "ekipahe",  "estutse",
"etika", "espiritu", "espiga",  "espinaka", "eskala", "eskilada", "eskinita",


"gigil","gunita","galak","galamay","gata",
"ganda","gunting","gantimpala","gantsilyo","garahe",
"gastos","gatas","gumiho","galit","gilas",
"giling","ginhawa","goma","granada","gwardya",
"guyod", "gutay", "gutli", "gulis", "gulod", "grasa",

"hita","harana","hilot","halohalo","halakhak",
"hiwaga","halimaw","haynayan","habol","hangarin",
"hagdan","hangin","haplos","hagulhol","hagupit",
"hakbang","halaga","halik","halili","halimuyak",
"hidhid", "himaton", "huwaran", "hukom",

"ilong","isda","indak", "itrangka",  "isangguni",
"ibon","ihip","ihaw","ilog","ilaw", "irog", "ipuipo",
"imbento","impok","ipon","indak","ipis",
"isa","isip","inip","isla","ispada", "ipilipil",

"kompyuter","kislap","kulisap","kalesa","kabayanihan",
"kalendaryo","kape","karne","kundiman","kalinaw",
"kamay","katawan","kababayan","kilig","kinaadman",
"kandungan","kalaban","kabisado","kaharian","kalayaan",
"kartamuneta",

"likha","labi","lapis","lalamunan","langgam",
"lambat","lamok","lantsa","lata","ligaya",
"likod","lawin","laro","liwanag","libre",
"likom","lolo","lobo","lugar","langoy",

"malapad", "mahaba","madilim","mabango",
"mabaho", "masama","malapit","mata","manok",
"mahirap","maganda","maliwanag","makisig","mailap",
"masaya","magaan","mesa","maneho","minuto", "miktinig",


"napakalaki","nakaw","ngiti","nakasayad","nasira",
"ngasiwa","ngayon","nginig","ngipin","nyog",
"nilalang","nuno", "nota", "nitso", "ninag",
"ningning",  "nilamukos", "nimsiha",  "nibelado",

"orasan","opinyon", "obeha", "obitwaryo", "obo", "oktubre", "orgulyo",
"otso", "oso", "oyayi",  "otsenta",  "orden",  "onda",  "onsa",

"pangit","paa","palayok","palaka","palautatan",
"purok","pista","panyo","pisngi","payong",
"palayok","pagsamo","pusa","paroparo","paraalan",
"pari","perlas","pikit","pera","pinya","puno",
"pooksapot",

"relo","ritwal","regalo","resibo","radyo",
"rayag", "reaksiyon", "rambutan", "ranggo",
"rapido", "ratiles",

"serbesa","segundo","sinag","sandok","sulit",
"suklay","sorbetes","sipnayan","susi","sindak",
"salipawpaw","simbahan","sipilyo","silid","samba",
"sinag","sando","sako","silyak","sakto", "silakbo",
"siyensya",

"tsaa","tubig","tela", "trangkaso","tambay",
"tainga","tadhana","tampo","tala","tsinelas",
"timpi","tanod","takipsilim","talaba","tuwalya",
"tigre","tindahan","takaw","twalya","tabo",
"tinatangi",

"yakis", "yagban", "yena", "yodin", "yoyo", "yugto",
"yamuyam", "yanga", "yanig", "yukayok",  "yumungyong",

"unggoy","ubo","ubas","udyok","ugali",
"ugat","ugnay","uhaw","ukit","ukol",
"ulianin","ulila","ulo","umaga","umay",
"unan","ulap","uniporme","upuan","utak" };


    public static void ReInitialize()
    {
        wordList_List = wordList_List = new List<string>
{  "albolaryo","alitaptap","abogado","alapaap","aparador",
"abaka","abaniko","abusado","agrikultura","ahas",
"agimat","aginaldo","agos","akda","aklat",
"alkalde","alon","apo","arko","aso", "awanggan" , "anluwage",
"abanderado", "abaloryo",  "abasto", "abitsuwelas",
"adhika", "abyog", "agawtulog", "agham",  "aghimuan",

"bayani","bulok","bibig","braso", "baboy","barangay","bughaw","bahin",
"balingkinitan","bahaghari","bahay","bisita", "bagabag",  "bagamundo",
"bulaklak","bukid","barko","baril", "bilnuran", "bagingan",  "bagisbis",
"balangay", "burdador", "bunyiin", "buntabay", "buntalain",  "bungkalin",
"bumulabog", "bumulahaw", "bumbon", "bumalagbag",  "buluble",

"diwa","dagat","dibdib","daliri","dalisay", "duta", "durado",
"digmaan","diwata","dalagita","dambana","damo",  "dusa",
"dila","disente","diyaryo","dugo","duwag", "dagalwak", "dagatkipot",
"duyan","disko","dighay","digmaan","dyosa", "durungawan", "duyog",

"empleyado","eskwelahan","elementarya","engkanto","estilo",
"elepante","elegante","eskenita","estado", "ehekutibo", "ehemplar",
"elesi", "enigma", "ektarya", "edukado", "ekipahe",  "estutse",
"etika", "espiritu", "espiga",  "espinaka", "eskala", "eskilada", "eskinita",


"gigil","gunita","galak","galamay","gata",
"ganda","gunting","gantimpala","gantsilyo","garahe",
"gastos","gatas","gumiho","galit","gilas",
"giling","ginhawa","goma","granada","gwardya",
"guyod", "gutay", "gutli", "gulis", "gulod", "grasa",

"hita","harana","hilot","halohalo","halakhak",
"hiwaga","halimaw","haynayan","habol","hangarin",
"hagdan","hangin","haplos","hagulhol","hagupit",
"hakbang","halaga","halik","halili","halimuyak",
"hidhid", "himaton", "huwaran", "hukom",

"ilong","isda","indak", "itrangka",  "isangguni",
"ibon","ihip","ihaw","ilog","ilaw", "irog", "ipuipo",
"imbento","impok","ipon","indak","ipis",
"isa","isip","inip","isla","ispada", "ipilipil",

"kompyuter","kislap","kulisap","kalesa","kabayanihan",
"kalendaryo","kape","karne","kundiman","kalinaw",
"kamay","katawan","kababayan","kilig","kinaadman",
"kandungan","kalaban","kabisado","kaharian","kalayaan",
"kartamuneta",

"likha","labi","lapis","lalamunan","langgam",
"lambat","lamok","lantsa","lata","ligaya",
"likod","lawin","laro","liwanag","libre",
"likom","lolo","lobo","lugar","langoy",

"malapad", "mahaba","madilim","mabango",
"mabaho", "masama","malapit","mata","manok",
"mahirap","maganda","maliwanag","makisig","mailap",
"masaya","magaan","mesa","maneho","minuto", "miktinig",


"napakalaki","nakaw","ngiti","nakasayad","nasira",
"ngasiwa","ngayon","nginig","ngipin","nyog",
"nilalang","nuno", "nota", "nitso", "ninag",
"ningning",  "nilamukos", "nimsiha",  "nibelado",

"orasan","opinyon", "obeha", "obitwaryo", "obo", "oktubre", "orgulyo",
"otso", "oso", "oyayi",  "otsenta",  "orden",  "onda",  "onsa",

"pangit","paa","palayok","palaka","palautatan",
"purok","pista","panyo","pisngi","payong",
"palayok","pagsamo","pusa","paroparo","paraalan",
"pari","perlas","pikit","pera","pinya","puno",
"pooksapot",

"relo","ritwal","regalo","resibo","radyo",
"rayag", "reaksiyon", "rambutan", "ranggo",
"rapido", "ratiles",

"serbesa","segundo","sinag","sandok","sulit",
"suklay","sorbetes","sipnayan","susi","sindak",
"salipawpaw","simbahan","sipilyo","silid","samba",
"sinag","sando","sako","silyak","sakto", "silakbo",
"siyensya",

"tsaa","tubig","tela", "trangkaso","tambay",
"tainga","tadhana","tampo","tala","tsinelas",
"timpi","tanod","takipsilim","talaba","tuwalya",
"tigre","tindahan","takaw","twalya","tabo",
"tinatangi",

"yakis", "yagban", "yena", "yodin", "yoyo", "yugto",
"yamuyam", "yanga", "yanig", "yukayok",  "yumungyong",

"unggoy","ubo","ubas","udyok","ugali",
"ugat","ugnay","uhaw","ukit","ukol",
"ulianin","ulila","ulo","umaga","umay",
"unan","ulap","uniporme","upuan","utak" };
    }

   


    public static string GetRandomWord ()
	{
        int list_length = wordList_List.Count;
        if (list_length <= 0)
            ReInitialize();

       
        int randomIndex = Random.Range(0, list_length);
		string randomWord = wordList_List[randomIndex];
        string returnRandomWord = randomWord;
        wordList_List.Remove(randomWord);

        

   
        
		return returnRandomWord;
	}

}
