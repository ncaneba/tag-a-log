---
| Name | Tag-A-Log |
|---|---|
| Description | A filipino typing game with a twist. |
| Authors | Nicohlus Ian R. Caneba __(ncaneba@gbox.adnu.edu.ph)__ |
|  | Janine Mae H. Llorca __(jmllorca@gbox.adnu.edu.ph)__ |
|  | Jan Rodolf P. Espinas __(jespinas@gbox.adnu.edu.ph)__ |
| Date Created | Sept 4, 2018 |
| Date Finished| October 7, 2018 |